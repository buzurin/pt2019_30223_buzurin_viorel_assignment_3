package PT2019.demo.DemoProject;

import java.awt.Dimension; 
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

public class Afisare extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JTable table;
	static JFrame frame = new JFrame();
	static Afisare a;
	static JTextField t1=new JTextField("");
	static JTextField t2=new JTextField("");
    static JTextField t3=new JTextField("");
    static JTextField t=new JTextField("");

	public Afisare(Object[][] data, String[] sc) {
		table = new JTable(data, sc);
		table.setPreferredScrollableViewportSize(new Dimension(500, 200));
		table.setFillsViewportHeight(true);
		JScrollPane scr = new JScrollPane(table);
		JPanel p = new JPanel();
		JButton b3 = new JButton("Inapoi");
		p.add(scr);
		p.add(b3);
		add(p);
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(true);
				a.setVisible(false);

			}
		});
	}

	public static void init() throws SQLException {
		// TODO Auto-generated method stub
		final Aple apel = new Aple();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 200);
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JButton b1 = new JButton("Afisare Clienti");
		JButton b2 = new JButton("Afisare Produse");
		JButton b3=new JButton("Adauga Client");
		JButton b4=new JButton("Adauga Produs");
		JButton b5=new JButton("Sterge Client");
		JButton b6=new JButton("Sterge Produs");
		JButton b7=new JButton("Editeaza Client");
		JButton b8=new JButton("Editeaza Produs");
		JButton b9=new JButton("Inapoi");
		p1.add(b9);
		p.setLayout(new GridLayout(4,4));
		p.add(b1);
		p.add(b2);
		p.add(b3);
		p.add(b4);
		p.add(b5);
		p.add(b6);
		p.add(b7);
		p.add(b8);
		b9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Meniu.frame.setVisible(true);
				frame.setVisible(false);
			}});
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					apel.afisClienti();
					a = new Afisare(apel.linie1, apel.coloana1);
					a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					a.setSize(600, 400);
					a.setVisible(true);
					frame.setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					apel.afisProduse();
					a = new Afisare(apel.linie1,apel.coloana1);
					a.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					a.setSize(600, 400);
					a.setVisible(true);
					frame.setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		});
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(3,2));
					JLabel l1=new JLabel("Nume:");
					JLabel l2=new JLabel("Prenume:");
					JLabel l3=new JLabel("Parola:");					
					JButton b=new JButton("Adauga");
					JButton b1=new JButton("Inapoi");
					p.add(l1);p.add(t1);
					p.add(l2);p.add(t2);
					p.add(l3);p.add(t3);
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						apel.adaugaClient(t1.getText(), t2.getText(), t3.getText());
						JOptionPane.showMessageDialog(null,"Client adaugat");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}
		});
		b7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(4,2));
					JLabel l=new JLabel("ID:");
					JLabel l1=new JLabel("Nume:");
					JLabel l2=new JLabel("Prenume:");
					JLabel l3=new JLabel("Parola:");					
					JButton b=new JButton("Modifica");
					JButton b1=new JButton("Inapoi");
					p.add(l);p.add(t);
					p.add(l1);p.add(t1);
					p.add(l2);p.add(t2);
					p.add(l3);p.add(t3);
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						if(apel.modificaClient(t1.getText(), t2.getText(), t3.getText(),Integer.parseInt(t.getText()))==1)
						 JOptionPane.showMessageDialog(null,"Client modificat");
						 else  JOptionPane.showMessageDialog(null,"Clientul nu a fost sters nume gresit sau nu exista");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}
		});
		b8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(4,2));
					JLabel l=new JLabel("ID:");
					JLabel l1=new JLabel("Nume:");
					JLabel l2=new JLabel("Cantitate:");
					JLabel l3=new JLabel("Pret:");					
					JButton b=new JButton("Modifica");
					JButton b1=new JButton("Inapoi");
					p.add(l);p.add(t);
					p.add(l1);p.add(t1);
					p.add(l2);p.add(t2);
					p.add(l3);p.add(t3);
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						if(apel.modificaProdus(t1.getText(),Integer.parseInt(t2.getText()),Float.parseFloat(t3.getText()),Integer.parseInt(t.getText()))==1)
						 JOptionPane.showMessageDialog(null,"Produs modificat");
						 else  JOptionPane.showMessageDialog(null,"Produsul nu a fost sters nume gresit sau nu exista");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}
		});
		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(3,2));
					JLabel l1=new JLabel("Nume:");
					JLabel l2=new JLabel("Cantitate:");
					JLabel l3=new JLabel("Pret:");					
					JButton b=new JButton("Adauga");
					JButton b1=new JButton("Inapoi");
					p.add(l1);p.add(t1);
					p.add(l2);p.add(t2);
					p.add(l3);p.add(t3);
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						apel.adaugaProdus(t1.getText(),Integer.parseInt(t2.getText()),Float.parseFloat(t3.getText()));
						JOptionPane.showMessageDialog(null,"Produs adaugat");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}			
		});
		b6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(3,2));
					JLabel l1=new JLabel("Nume:");										
					JButton b=new JButton("Sterge");
					JButton b1=new JButton("Inapoi");
					p.add(l1);p.add(t1);					
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						if(apel.stergeProduse(t1.getText())==1)
						 JOptionPane.showMessageDialog(null,"Produs sters");
						else  JOptionPane.showMessageDialog(null,"Produsul nu a fost sters nume gresit sau nu exista");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}});
		b5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {				
					final JFrame frame1=new JFrame();
					frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					frame1.setSize(500, 200);			
					JPanel p=new JPanel();
					JPanel p1=new JPanel();
					JPanel p3=new JPanel();
					p.setLayout(new GridLayout(3,2));
					JLabel l1=new JLabel("ID Client:");										
					JButton b=new JButton("Sterge");
					JButton b1=new JButton("Inapoi");
					p.add(l1);p.add(t1);					
					p1.add(b);
					p1.add(b1);
					b.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
					  try {						
						if(apel.stergeClient(Integer.parseInt(t1.getText()))==1)
						 JOptionPane.showMessageDialog(null,"Client sters");
						else  JOptionPane.showMessageDialog(null,"Clientul nu a fost sters nume gresit sau nu exista");
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
						}});
					b1.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							frame.setVisible(true);
							frame1.setVisible(false);
						}
					});
					p3.setLayout(new BoxLayout(p3, BoxLayout.Y_AXIS));
					p3.add(p);
					p3.add(p1);
					frame1.add(p3);					
					frame1.setVisible(true);
					frame.setVisible(false);
			}});
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		p2.add(p);
		p2.add(p1);
		frame.add(p2);
		frame.setVisible(true);
	}

}
