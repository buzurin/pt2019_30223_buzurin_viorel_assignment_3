package PT2019.demo.DemoProject;
import java.sql.CallableStatement; 
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Aple {
	
	public String [] coloana1;
	public Object [][] linie1 =new Object[20][20];
	public void afisClienti() throws SQLException {
		String [] coloana;
	    Object [][] linie =new Object[20][20];
		JdbcExample.connection();
		ResultSet rs;
		Statement stmt = JdbcExample.connection.createStatement();
		String sql2 = "select * from client";
		rs = stmt.executeQuery(sql2);
		coloana=new String[] {"id", "nume", "prenume"};
		int i=0,j=0;
		while (rs.next()) {
			linie[i][j] = rs.getString("id");
			j++;
			linie[i][j] = rs.getString("nume");
			j++;
			linie[i][j] = rs.getString("prenume");	
			i++;
			j=0;			
		}
		coloana1=coloana;
		linie1=linie;
	}
	public void afisProduse() throws SQLException {
		String [] coloana;
	    Object [][] linie =new Object[20][20];
		JdbcExample.connection();
		ResultSet rs;
		Statement stmt = JdbcExample.connection.createStatement();
		String sql2 = "select * from produs";
		rs = stmt.executeQuery(sql2);
		coloana=new String[] { "id","nume","cantitate","pret"};
		int i=0,j=0;
		while (rs.next()) {
			linie[i][j] = rs.getString("id");j++;
			linie[i][j] = rs.getString("nume");j++;
			linie[i][j] = rs.getString("cantitate");j++;
			linie[i][j]=  rs.getString("pret");
			j=0;
			i++;			
		}
		coloana1=coloana;
		linie1=linie;
	}
	public void adaugaProdus(String nume,int cantitate,float pret) throws SQLException {
		JdbcExample.connection();		
		String sql = "{ CALL adauga_produs(?,?,?)} ";	
		 CallableStatement cs =  JdbcExample.connection.prepareCall(sql);
		 cs.setString(1, nume);
		 cs.setInt(2, cantitate);
		 cs.setFloat(3,pret);		 					
		cs.execute();		
	}
	public void adaugaClient(String nume,String prenume,String parola) throws SQLException {
		JdbcExample.connection();		
		String sql = "{ CALL adauga_client(?,?,?)} ";	
		 CallableStatement cs =  JdbcExample.connection.prepareCall(sql);
		 cs.setString(1,nume);
		 cs.setString(2,prenume);
		 cs.setString(3,parola);		 					
		cs.execute();		
	}
	public int stergeProduse(String name) throws SQLException {		
		JdbcExample.connection();		
		Statement stmt = JdbcExample.connection.createStatement();		
		int i = stmt.executeUpdate("DELETE FROM produs WHERE nume=" + "'"+name+"'");
		if(i==1)
			return 1;
		else return 0;
	}
	public int stergeClient(int id) throws SQLException {		
		JdbcExample.connection();		
		Statement stmt = JdbcExample.connection.createStatement();		
		int i = stmt.executeUpdate("DELETE FROM client WHERE id=" + id);
		if(i==1)
			return 1;
		else return 0;
	}
	public int modificaClient(String nume,String prenume,String parola,int id) throws SQLException {		
		JdbcExample.connection();			
		java.sql.PreparedStatement ps =JdbcExample.connection.prepareStatement("UPDATE client SET nume=?, prenume=?, parola=? WHERE id=?");
        ps.setString(1, nume);
        ps.setString(2, prenume);
        ps.setString(3, parola); 
        ps.setInt(4, id);
        int i = ps.executeUpdate();
		if(i==1)
			return 1;
		else return 0;
	}
	public int modificaProdus(String nume,int cantitate,float pret,int id) throws SQLException {		
		JdbcExample.connection();			
		java.sql.PreparedStatement ps =JdbcExample.connection.prepareStatement("UPDATE produs SET nume=?, cantitate=?, pret=? WHERE id=?");
        ps.setString(1, nume);
        ps.setInt(2, cantitate);
        ps.setFloat(3, pret); 
        ps.setInt(4, id);
        int i = ps.executeUpdate();
		if(i==1)
			return 1;
		else return 0;
	}
	public void adaugaComanda(int id_c, int id_p,float pret) throws SQLException {
		JdbcExample.connection();		
		String sql = "{ CALL adauga_comanda(?,?,?)} ";	
		 CallableStatement cs =  JdbcExample.connection.prepareCall(sql);
		 cs.setInt(1,id_c);
		 cs.setInt(2,id_p);
		 cs.setFloat(3,pret);		 					
		cs.execute();		
	}
	
}
