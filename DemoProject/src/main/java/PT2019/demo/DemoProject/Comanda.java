package PT2019.demo.DemoProject;

import java.awt.Color;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Comanda {	
	static float pret_p=0;
	static String []nume=new String[30];
	static int []cantitate=new int[100];
	static float []pret=new float[100];
	static int []id_prod=new int[100];
	static JTextField t2=new JTextField("");	
	static JTextField t3=new JTextField("");
	static JTextField t1=new JTextField("");
	public String produse=new String();
	public int id_a=0;	
	public static int fac=0;
	public  void init(final int id) throws SQLException
	{
	    final JFrame frame=new JFrame();		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(700, 500);
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JLabel l1=new JLabel("ID:");
		JLabel l2=new JLabel("Nume:");
		JLabel l3=new JLabel("Prenume:");
		JTextField t1=new JTextField("  "+id+"  ");			
		t1.setEditable(false);		
		JButton b1 = new JButton("Comanda");	
		JButton b2 = new JButton("Inapoi");	
		JButton b4 = new JButton("Alege");			
		JDesktopPane panel = new JDesktopPane();
		JButton b3 = new JButton("Adauga");
		b3.setBounds(311, 234, 97, 25);
		panel.add(b3);	
		
		t2.setEditable(false);
		t3.setEditable(false);
		t1.setText("  "+id+" ");			
		t1.setEditable(false);		
		JdbcExample.connection();
		ResultSet rs;
		ResultSet rs1;
		Statement stmt = JdbcExample.connection.createStatement();
		String sql2 = "select nume,prenume from client where id="+id;
		rs = stmt.executeQuery(sql2);		
		if (rs.first()) {			
			t2.setText( rs.getString("nume"));
			t3.setText(rs.getString("prenume"));						
		}		
		int i=0;
		String sql = "select id,nume,cantitate,pret from produs";
		rs1 = stmt.executeQuery(sql);		
		while (rs1.next()) {			
				nume[i]=rs1.getString("nume");
				cantitate[i]=Integer.parseInt(rs1.getString("cantitate"));
				pret[i]=Float.parseFloat(rs1.getString("pret"));
				id_prod[i]=Integer.parseInt(rs1.getString("id"));
				i++;
		}		
		
		b4.setBounds(570, 90, 97, 25);
		panel.add(b4);	
		
		JLabel lb5 = new JLabel("Cos Produse:");
		lb5.setBounds(37, 70,  100, 16);
		panel.add(lb5);
		
		final List list_1 = new List();
		list_1.setBounds(37, 90, 246, 262);
		panel.add(list_1);
		
		
		
		final JComboBox<String> choice = new JComboBox<String>(nume);
		choice.setFont(null);
		choice.setSelectedItem(null);		
		choice.setBackground(Color.GRAY);
		choice.setBounds(311, 90, 246, 22);
		panel.add(choice);			
		
		JLabel lb = new JLabel("Pret produs:");
		lb.setBounds(310, 125, 100, 16);
		panel.add(lb);
		
		JLabel lb1 = new JLabel("Cantitate");
		lb1.setBounds(310, 155, 56, 16);
		panel.add(lb1);
		
		final JLabel lb2 = new JLabel("0");
		lb2.setBounds(391, 125, 56, 16);
		panel.add(lb2);
		
		final JLabel lb3 = new JLabel("0");
		lb3.setBounds(391, 155, 56, 16);
		panel.add(lb3);	
		
		JLabel lb6 = new JLabel("Pret Total:");
		lb6.setBounds(310, 185, 80, 16);
		panel.add(lb6);
		
		final JLabel lb7 = new JLabel("0");
		lb7.setBounds(391, 185, 56, 16);
		panel.add(lb7);	
		b4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {		
				lb2.setText(""+pret[choice.getSelectedIndex()]);	
				lb3.setText(""+cantitate[choice.getSelectedIndex()]);
		}});
	final Aple apel=new Aple();
		b3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(cantitate[choice.getSelectedIndex()]>0) {				
				pret_p=pret_p+pret[choice.getSelectedIndex()];
				lb7.setText(""+pret_p);
				produse=produse+"   "+nume[choice.getSelectedIndex()]+" Pret:"+pret[choice.getSelectedIndex()]+"  ID:"+id_prod[choice.getSelectedIndex()];				
				id_a=id_a*10+id_prod[choice.getSelectedIndex()];
				list_1.add(nume[choice.getSelectedIndex()]+" Pret:"+pret[choice.getSelectedIndex()]+"  ID:"+id_prod[choice.getSelectedIndex()]);
				cantitate[choice.getSelectedIndex()]--;
				try {					
					apel.modificaProdus(nume[choice.getSelectedIndex()], cantitate[choice.getSelectedIndex()],pret[choice.getSelectedIndex()],id_prod[choice.getSelectedIndex()]);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
				else 
				{
					 JOptionPane.showMessageDialog(null,"Nu mai sunt produse");
					 try {
						apel.stergeProduse(nume[choice.getSelectedIndex()]);
						choice.remove(choice.getSelectedIndex());
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();						
					}
				}
			}});
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);			
				Meniu.frame.setVisible(true);				
				pret_p=0;				
			}});
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {					
					apel.adaugaComanda(id, id_a, pret_p);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Writer writer = null;
				fac++;
				try {
				    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Factura"+fac+".txt"), "utf-8"));
				    writer.write("Nume:"+t2.getText()+"     Prenume:"+t3.getText());
				    ((BufferedWriter) writer).newLine();
				    writer.write(produse);
				    ((BufferedWriter) writer).newLine();
				    writer.write("Pret Total Factura="+pret_p);
				} catch (IOException ex) {
				    // Report
				} finally {
				   try {writer.close();} catch (Exception ex) {/*ignore*/}
				}
			}});
		p.add(l1);p.add(t1);
		p.add(l2);p.add(t2);
		p.add(l3);p.add(t3);
		p1.add(b1);p1.add(b2);
		p2.add(p);			
		p2.add(panel);
		p2.add(p1);			
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));		
		frame.add(p2);
		frame.setVisible(true);
	}
}
