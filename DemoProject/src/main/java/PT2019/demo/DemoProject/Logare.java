package PT2019.demo.DemoProject;

import java.awt.GridLayout; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;



public class Logare {
	static JFrame frame=new JFrame();
	static Comanda a;
	public void init()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 200);
		JPanel p = new JPanel();
		JPanel p1 = new JPanel();
		JPanel p2 = new JPanel();
		JLabel l1=new JLabel("ID:");
		JLabel l2=new JLabel("Parola:");
		final JTextField t=new JTextField("");
		final JPasswordField pas = new JPasswordField();
		JButton b1 = new JButton("Logare");	
		JButton b2 = new JButton("Inapoi");	
		p.add(l1);p.add(t);
		p.add(l2);p.add(pas);
		p1.add(b1);p1.add(b2);
		p2.add(p);
		p2.add(p1);
		p.setLayout(new GridLayout(2,2));
		p2.setLayout(new BoxLayout(p2, BoxLayout.Y_AXIS));
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.setVisible(false);
				Meniu.frame.setVisible(true);							
			}});
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JdbcExample.connection();
				 PreparedStatement ps;
				try {
					ps = (PreparedStatement) JdbcExample.connection.prepareStatement("SELECT * FROM client WHERE id=? AND parola=?");				
					ps.setInt(1, Integer.parseInt(t.getText()));					
			        ps.setString(2, pas.getText());
			        ResultSet rs = ps.executeQuery();
			        if(rs.next())
			        {
			        	 JOptionPane.showMessageDialog(null,"Date corecte");
			        	 frame.setVisible(false);
			        	 a=new Comanda();
			        	 a.init(Integer.parseInt(t.getText()));			        	
			        	 t.setText("");
			        	 pas.setText("");
			        }
			        else
			        	 JOptionPane.showMessageDialog(null,"Date incorecte");
				}
				 catch (SQLException ex) {
				        ex.printStackTrace();
				    }
			}});
		frame.add(p2);
		frame.setVisible(true);
	}


}
