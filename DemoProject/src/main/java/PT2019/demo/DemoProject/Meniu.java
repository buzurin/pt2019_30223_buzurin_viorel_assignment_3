package PT2019.demo.DemoProject;

import java.awt.event.ActionEvent; 
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Meniu {
	static JFrame frame=new JFrame();
	static int i=0;
	public static void main(String[] args){
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(500, 200);
		JPanel p = new JPanel();
		JButton b1 = new JButton("Comanda");
		JButton b2 = new JButton("Informatie");
		p.add(b1);
		p.add(b2);
		b2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Afisare.init();
					Meniu.frame.setVisible(false);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}				
			}});
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					Logare l=new Logare();
					l.init();
					Meniu.frame.setVisible(false);							
			}});		
		frame.add(p);
		frame.setVisible(true);
	}
}
